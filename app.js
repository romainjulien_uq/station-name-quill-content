var readline = require('readline');
var fs = require('fs');

/**
 * This function is called with the list of names extracted from the file.
 * It will either output the minimum list of names containing all letters in the alphabet, or
 * an error message.
 *
 * @param listNames List of station names
 */
var start = function(listNames) {
    var remainingLetters = 'abcdefghijklmnopqrstuvwxyz';

    try {
        var result = process(listNames, remainingLetters, []);
        console.log('result: ' + result);
    } catch(error) {
        console.log(error.message);
    }

};

/**
 * A recursive method, called until a list of names containing all letters in the alphabet has been found.
 * The algorithm is as follow:
 * - Trim the station names to consider only the letters that are still to be found, and ignore
 * duplicated letters in a name. We map each names to their relevant letters (at this stage/method call), and only
 * the relevant letters will be considered for the rest of this call
 * - Find the number of occurences of each relevant letter among all names
 * - Compute the score for each name, and return the highest score:
 * score = 1/[nbr occurences 1st relevant letter] + 1/[nbr occurences 2nd letter] + ...
 * - Remove the relevant letters of the returned name from the list of remaining letters to be found
 * - Remove the returned name from the list of names
 *
 * This method throws an error if the given list of names does not contains all letters in the alphabet.
 *
 * @param remainingNames - List of names that havnt been selected yet
 * @param remainingLetters - List of letters that are not in the names selected so far
 * @param result - List of names selected so far
 * @returns {*} Minimum list of selected names containing all letters in the alphabet
 */
var process = function(remainingNames, remainingLetters, result) {
    if (remainingLetters.length === 0) {
        return result;
    }

    // If there is still letters to be found, but all names in the list have been selected already, throw an error
    if (remainingNames.length === 0) {
        throw new Error(
            'The names provided do not contain all letters in the alphabet\n' +
            'Missing letters: ' + remainingLetters);
    }

    // Pre process to find the name with highest score
    var relevantLettersInName = getRelevantLettersInNames(remainingLetters, remainingNames);
    var letterCount = getLetterCount(relevantLettersInName);

    // Find name with highest score
    var nameWithHighestScore = getNameWithHighestScore(relevantLettersInName, letterCount);

    // Post process given name with highest score
    var newRemainingLetters = removeLettersFromString(remainingLetters, relevantLettersInName[nameWithHighestScore]);
    var newRemainingNames = removeStringFromList(remainingNames, nameWithHighestScore);

    result.push(nameWithHighestScore);

    return process(newRemainingNames, newRemainingLetters, result);
};

/**
 * For each names, check to see if each letters is relevant, ie remove duplicated letters
 * and letters already found.
 *
 * @param remainingLetters - Letters that are still to be found
 * @param names - List of remaining names
 * @returns {{}} - A mapping: {name: relevant letters in name}
 */
var getRelevantLettersInNames = function(remainingLetters, names) {
    var relevantLettersInNames = {};

    names.forEach(function(name) {
        var r = '';

        for (var i = 0; i < name.length; i++) {
            var c = name.charAt(i);
            // if remainingLetters contains c and c hasnt been found in the name already
            if (remainingLetters.indexOf(c) !== -1  && r.indexOf(c) === -1) {
                r += c;
            }
            relevantLettersInNames[name] = r;
        }

    });

    return relevantLettersInNames;
};

/**
 * Iterate through each names (relevant letters only) and count the number of occurences of each letters
 * @param relevantLettersInName - A mapping: {name: relevant letters in name}
 * @returns {{}} - A mapping: {letter: number of occurences among all names}
 */
var getLetterCount = function(relevantLettersInName) {
    var letterCount = {};

    for(var key in relevantLettersInName) {
        var trimedName = relevantLettersInName[key];
        for (var i = 0; i < trimedName.length; i++) {
            var c = trimedName.charAt(i);

            if (letterCount[c] === undefined) {
                letterCount[c] = 1;
            } else {
                letterCount[c]++;
            }
        }
    }

    return letterCount;
};

/**
 * Compute the score for each name, given its relevant letters and the letter count,
 * score = 1/[count 1st relevant letter] + 1/[count 2nd letter] + ...
 * Kepp track of the highest score and the corresponding name.
 *
 * @param relevantLettersInName - A mapping: {name: relevant letters in name}
 * @param letterCount - A mapping: {letter: number of occurences among all names}
 * @returns {string} - The name with highest score
 */
var getNameWithHighestScore = function(relevantLettersInName, letterCount) {

    var result = '';
    var maxScore = 0;

    for(var key in relevantLettersInName) {
        var trimedName = relevantLettersInName[key];
        var nameScore = 0.0;

        for (var i = 0; i < trimedName.length; i++) {
            var c = trimedName.charAt(i);
            var cCount = letterCount[c];
            nameScore += 1/cCount;
        }

        if (nameScore > maxScore) {
            maxScore = nameScore;
            result = key;
        }
    }

    return result;
};

/**
 * Remove the letters in lettersToRemove from str.
 *
 * @param str - A String
 * @param lettersToRemove - Letters to remove from the string
 * @returns {*} - str after letters have been removed
 */
var removeLettersFromString = function(str, lettersToRemove) {
    for (var i = 0; i < lettersToRemove.length; i++) {
        str = str.replace(lettersToRemove.charAt(i), '');
    }
    return str;
};

/**
 * Remove a String from a list of names.
 *
 * @param listNames - A list of name (String)
 * @param strToRemove - The String to find and remove
 * @returns {*} listNames after the string has been removed
 */
var removeStringFromList = function(listNames, strToRemove) {
    for (var i = 0; i < listNames.length; i++) {
        if (listNames[i] === strToRemove) {
            listNames.splice(i, 1);
            break;
        }
    }
    return listNames;
};

/**
 * Extract each name on a line and store it in an array.
 * The array of names is then passed to the start method.
 */
var getListNames = function() {
    var listNames = [];

    var rl = readline.createInterface({
        input: fs.createReadStream('names.txt')
    });

    rl.on('line', function (name) {
        listNames.push(name.toLowerCase());
    });

    rl.on('close', start.bind(this, listNames));
};

// ----------------------------------------------------------------------------- //
// ---------------------------------- ENTRY POINT ------------------------------ //
// ----------------------------------------------------------------------------- //

/**
 * The entry point of the application
 */
getListNames();