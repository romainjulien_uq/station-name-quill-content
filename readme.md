Setting Up
========================

## Author(s)
*   Romain Julien

--------------------------------------------------------------------------------
## Getting started
##### These instructions are for setup using the Intellij IDE 
1. Clone the repository from the following location
    https://romainjulien_uq@bitbucket.org/romainjulien_uq/station-name-quill-content.git
    
2. Make sure you have node and npm installed
    `brew install node`

3. Start the node js application
`npm start`

When you issue the command `npm start` from the root directory of your nodejs project, 
node will look for a scripts (start) object in your package.json file. 
